# Game Of Ur

## Name
The Royal Game of Ur

## Description
This is a library for the anceint Royal Game of Ur.

***

## Installation

```
git clone https://gitlab.com/TheWalkingForest/game-of-ur.git
cd game-of-ur
cargo build --release
```

## Usage
Use this library to as the backend for a Royal Game of Ur application.

## License
This code in this repository is licensed under the MIT license.

## Project status
Currently under development in my spare time.  Updates on no set schedule.
