use ansi_term::{Color, Style};
use board::Board;
use player::Player;
use rand::prelude::*;
use rustyline::error::ReadlineError;
use std::fmt;

pub mod board;
pub mod piece;
pub mod player;

#[derive(Clone, Copy, Debug)]
pub enum Team {
    Red,
    Blue,
}

impl fmt::Display for Team {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

fn roll() -> u32 {
    let mut rng = rand::thread_rng();
    [
        rng.gen_range(0..=1),
        rng.gen_range(0..=1),
        rng.gen_range(0..=1),
        rng.gen_range(0..=1),
    ]
    .iter()
    .sum()
}

fn select_piece() -> Option<u32> {
    let mut rl = rustyline::Editor::<()>::new();
    let readline = rl.readline("Select piece to move (0 to pass): ");
    match readline {
        Ok(line) => match line.parse() {
            Ok(num) if (num <= 7 && num >= 1) => return Some(num),
            Ok(0) => println!("=> Passes"),
            Ok(_) => println!("=> Not a Piece"),
            Err(_) => println!("=> Not a number"),
        },
        Err(ReadlineError::Interrupted) => {
            /* Control-C */
            std::process::exit(0x0100)
        }
        Err(ReadlineError::Eof) => {
            /* Control-D */
            std::process::exit(0x0100)
        }
        Err(err) => {
            println!("Error: {:?}", err);
            std::process::exit(0x0100);
        }
    }
    None
}

fn move_piece(
    board: &mut Board,
    curr_player: &mut Player,
    opp_player: &mut Player,
    roll: u32,
    selection: usize,
) -> Result<(), String> {
    match board.update(curr_player, opp_player, roll, selection) {
        Ok(_) => Ok(()),
        // Rethrowing error up to ur::turn()
        Err(e) => Err(e),
    }
}

fn turn(board: &mut Board, curr_player: &mut Player, opp_player: &mut Player) -> Option<Team> {
    let roll = roll();
    println!("Roll: {}", roll);

    curr_player.show_pieces();
    loop {
        let selection: u32 = loop {
            if let Some(line) = select_piece() {
                break line;
            }
        };
        match move_piece(board, curr_player, opp_player, roll, selection as usize - 1) {
            Ok(_) => break,
            Err(err) => eprintln!("{}", err),
        };
    }

    return if curr_player.score() == 7 {
        Some(curr_player.team)
    } else {
        None
    };
}

pub fn run() {
    let mut game_board = Board::new();
    let mut player_red = Player::new(Team::Red);
    let mut player_blue = Player::new(Team::Blue);

    let mut ticker = 0;

    let mut win = None::<Team>;
    let winner = loop {
        // Clears screen and places cursor at the top left
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);

        println!("{}", game_board.draw_board());

        let team_style: Style = match ticker {
            0 => Style::new().bold().underline().fg(Color::Red),
            1 => Style::new().bold().underline().fg(Color::Blue),
            _ => Style::new().fg(Color::White),
        };
        match ticker {
            0 => {
                // Red player's turn
                println!("{}", team_style.paint(String::from("Red's Turn")));
                win = turn(&mut game_board, &mut player_red, &mut player_blue);
            }
            1 => {
                // Blue player's turn
                println!("{}", team_style.paint(String::from("Blue's Turn")));
                win = turn(&mut game_board, &mut player_blue, &mut player_red);
            }
            _ => {}
        }
        if win.is_some() {
            break win;
        }
        ticker = (ticker + 1) % 2;
    };

    let win_style = Style::new().bold().underline().fg(Color::Green);
    let err_style = Style::new().bold().underline().fg(Color::Red);
    match winner {
        Some(Team::Red) => println!("{}", win_style.paint("Red Player Wins!!")),
        Some(Team::Blue) => println!("{}", win_style.paint("Blue Player Wins!!")),
        _ => eprintln!("{}", err_style.paint("=> ERROR: No player won")),
    }
}

#[test]
fn roll_test() {
    for _ in 0..100 {
        assert!(roll() <= 4);
    }
}
