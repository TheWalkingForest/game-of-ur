use crate::ur::piece::Piece;
use crate::ur::player::Player;
use crate::ur::Team;
use std::vec::Vec;

#[derive(Clone, Copy, Debug)]
pub enum Space {
    Stone(Piece),
    Flower,
    Space,
    Empty,
}

#[derive(Clone)]
pub struct Board {
    board: Vec<Vec<Space>>,
}

impl Board {
    pub fn new() -> Board {
        let mut board = Board {
            board: vec![vec![Space::Empty; 8]; 3],
        };

        for i in 0..3 {
            if i == 0 || i == 2 {
                for j in 0..8 {
                    board.board[i][j] = if let 4 | 6 = j {
                        Space::Flower
                    } else if let 0 | 7 = j {
                        Space::Empty
                    } else {
                        Space::Space
                    };
                }
            } else {
                for j in 0..8 {
                    board.board[i][j] = if let 3 = j {
                        Space::Flower
                    } else {
                        Space::Space
                    };
                }
            }
        }
        board
    }

    pub fn draw_board(&self) -> String {
        let out_line = "+---+---+---+---+       +---+---+";
        let inner_line = "+---+---+---+---+---+---+---+---+";

        let draw_space = |space_type: &Space| -> String {
            match space_type {
                Space::Stone(Piece {
                    team: Team::Red, ..
                }) => return "| R ".to_string(),
                Space::Stone(Piece {
                    team: Team::Blue, ..
                }) => return "| B ".to_string(),
                Space::Flower => return "| * ".to_string(),
                Space::Space => return "|   ".to_string(),
                Space::Empty => return "    ".to_string(),
            }
        };

        // outer line for the board
        let mut board = format!("{}\n", out_line);

        for row in 0..self.board.len() {
            // dividing line between rows
            if row == 1 || row == 2 {
                board = format!("{}{}\n", board, inner_line);
            }

            // drawing spaces for each row
            if row == 0 || row == 2 {
                // print first 5 spaces in correct order
                for i in (0..=4).rev() {
                    board = format!("{}{}", board, draw_space(&self.board[row][i]));
                }

                // print last 3 spaces
                for i in (5..=7).rev() {
                    board = format!("{}{}", board, draw_space(&self.board[row][i]));
                }
            } else {
                for space in &self.board[row] {
                    board = format!("{}{}", board, draw_space(&space));
                }
            }
            board = format!("{}\n", board);
        }

        // outer line for the board
        board = format!("{}{}", board, out_line);
        board
    }

    pub fn update(
        &mut self,
        curr_player: &mut Player,
        opp_player: &mut Player,
        roll: u32,
        selection: usize,
    ) -> Result<(), String> {
        let last_pos = curr_player.pieces[selection].location;
        let next_pos = curr_player.pieces[selection].location + roll as usize;

        match curr_player.pieces[selection].update_position(roll) {
            // Rethrowing error up to ur::move_piece()
            Err(e) => return Err(e),
            _ => {}
        }

        // Updating pieces position on board_space
        if next_pos >= 13 && next_pos < 16 {
            self.board[match curr_player.team {
                Team::Red => 0,
                Team::Blue => 2,
            }][next_pos - 8] = Space::Stone(curr_player.pieces[selection].clone());
        } else if next_pos < 5 {
            self.board[match curr_player.team {
                Team::Red => 0,
                Team::Blue => 2,
            }][next_pos] = Space::Stone(curr_player.pieces[selection].clone());
        } else {
            self.board[1][next_pos - 5] = Space::Stone(curr_player.pieces[selection].clone());
        }

        // restoring last spot to correct space after piece has moved
        match last_pos {
            0 => { /* starting space doesnt need to be updated, always empty */ }
            4 | 14 => {
                self.board[match curr_player.team {
                    Team::Red => 0,
                    Team::Blue => 2,
                }][match last_pos {
                    4 => 4,
                    14 => 6,
                    _ => return Err("=> Unable to update board position".to_string()),
                }] = Space::Flower;
            }
            8 => self.board[1][3] = Space::Flower,
            1..=3 => {
                self.board[match curr_player.team {
                    Team::Red => 0,
                    Team::Blue => 2,
                }][last_pos] = Space::Space;
            }
            5..=12 => {
                self.board[1][last_pos - 8] = Space::Space;
            }
            13 => {
                self.board[match curr_player.team {
                    Team::Red => 0,
                    Team::Blue => 2,
                }][last_pos - 8] = Space::Space;
            }
            _ => return Err("=> Unable to update board position".to_string()),
        };

        Ok(())
    }
}

#[test]
fn draw_board_empty_test() {
    let new_board = Board::new();
    let test_board = "+---+---+---+---+       +---+---+\n| * |   |   |           | * |   \n+---+---+---+---+---+---+---+---+\n|   |   |   | * |   |   |   |   \n+---+---+---+---+---+---+---+---+\n| * |   |   |           | * |   \n+---+---+---+---+       +---+---+".to_string();
    assert_eq!(new_board.draw_board(), test_board)
}
