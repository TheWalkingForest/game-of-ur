use crate::ur::Team;

#[derive(Clone, Copy, Debug)]
pub struct Piece {
    pub team: Team,
    pub location: usize,
}

impl Piece {
    pub fn new(team: Team) -> Piece {
        Piece { team, location: 0 }
    }

    pub fn update_position(&mut self, positions: u32) -> Result<(), String> {
        if self.location + positions as usize > 15 {
            Err("=> Piece location could not be updated".to_string())
        } else {
            self.location += positions as usize;
            Ok(())
        }
    }
}
