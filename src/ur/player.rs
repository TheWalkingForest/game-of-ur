use crate::ur::piece::Piece;
use crate::ur::Team;
use ansi_term::{ANSIGenericString, Color, Style};

#[derive(Debug)]
pub struct Player {
    pub pieces: Vec<Piece>,
    pub team: Team,
}

impl Player {
    pub fn new(team: Team) -> Player {
        Player {
            pieces: vec![Piece::new(team); 7],
            team,
        }
    }

    pub fn show_pieces(&self) {
        let done_style = Style::new().fg(Color::Green);
        let start_style = Style::new().fg(Color::Yellow);
        let on_board_style = Style::new().fg(Color::White);

        let pos_style = |p: usize| -> ANSIGenericString<_> {
            if p == 15 {
                done_style.paint(p.to_string())
            } else if p == 0 {
                start_style.paint(p.to_string())
            } else {
                on_board_style.paint(p.to_string())
            }
        };

        let mut count = 1;
        for p in self.pieces.iter() {
            println!(
                "piece: {pce} | position: {pos}",
                pce = count,
                pos = pos_style(p.location)
            );
            count += 1;
        }

        println!("Score: {}", self.score());
    }

    pub fn score(&self) -> u32 {
        let mut count = 0;
        for p in self.pieces.iter() {
            if p.location == 15 {
                count += 1;
            }
        }
        count
    }
}

#[test]
fn player_test() {
    let red = Player::new(Team::Red);
    let red_test = Player {
        pieces: vec![Piece::new(Team::Red); 7],
        team: Team::Red,
    };
}
